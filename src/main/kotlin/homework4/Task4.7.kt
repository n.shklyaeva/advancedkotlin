package homework4

fun main() {
    println("Определите количество людей для генерации персональных данных. Введите число от 0 до 30 включительно: ")
    val number = getNumberOfPersons()

    println("Сгенерировать персональных данных: $number")
    val persons = DataGenerator().generate(number)
    JsonFileGenerator(persons).generate()
    PdfFileGenerator(persons).generate()
}
fun getNumberOfPersons(): Int {
    var n: Int
    while (true) {
        try {
            n = readln().toInt()
            if (n !in 1..30) {
                println("Введите, пожалуйста, число от 1 до 30.")
                continue
            }
            break
        } catch (e: NumberFormatException) {
            println("Введите, пожалуйста, число.")
        }
    }
    return n
}