package homework4

import kotlinx.serialization.Serializable

@Serializable
data class Person(
    val firstName: String,
    val surname: String,
    val patronymic: String,
    val age: Int,
    val gender: Gender,
    val dateOfBirth: String,
    val placeOfBirth: String,
    val inn: String,
    val address: Address
)