package homework4

import kotlinx.serialization.Serializable

@Serializable
data class Address(
    val index: String,
    val country: String,
    val area: String,
    val city: String,
    var street: String,
    val house: String,
    val flat: String
)