package homework4

import com.lowagie.text.*
import com.lowagie.text.pdf.PdfPCell
import com.lowagie.text.pdf.PdfPTable
import com.lowagie.text.pdf.PdfWriter
import org.librepdf.openpdf.fonts.Liberation
import java.io.FileOutputStream
import java.io.IOException

class PdfFileGenerator(private val persons: MutableList<Person>) : FileGenerator {
    override fun generate() {
        createOutputDir()
        val pdfDoc = Document(PageSize.getRectangle(A3_ALBUM))
        try {
            PdfWriter.getInstance(pdfDoc, FileOutputStream(PDF_FILENAME));
            pdfDoc.open()
            val table = PdfPTable(columns.values.toFloatArray())
            table.horizontalAlignment = 0

            for (name in columns.keys) {
                table.addCell(PdfPCell(Phrase(name, fontHeader)))
            }
            for (person in persons) {
                table.addCell(PdfPCell(Phrase(person.firstName, fontCell)))
                table.addCell(PdfPCell(Phrase(person.surname, fontCell)))
                table.addCell(PdfPCell(Phrase(person.patronymic, fontCell)))
                table.addCell(PdfPCell(Phrase(person.age.toString(), fontCell)))
                table.addCell(PdfPCell(Phrase(person.gender.naming, fontCell)))
                table.addCell(PdfPCell(Phrase(person.dateOfBirth, fontCell)))
                table.addCell(PdfPCell(Phrase(person.placeOfBirth, fontCell)))
                table.addCell(PdfPCell(Phrase(person.inn, fontCell)))
                table.addCell(PdfPCell(Phrase(person.address.index, fontCell)))
                table.addCell(PdfPCell(Phrase(person.address.country, fontCell)))
                table.addCell(PdfPCell(Phrase(person.address.area, fontCell)))
                table.addCell(PdfPCell(Phrase(person.address.city, fontCell)))
                table.addCell(PdfPCell(Phrase(person.address.street, fontCell)))
                table.addCell(PdfPCell(Phrase(person.address.house, fontCell)))
                table.addCell(PdfPCell(Phrase(person.address.flat, fontCell)))
            }

            pdfDoc.add(table)
        } catch (e: DocumentException) {
            println(e.message)
        } catch (e: IOException) {
            println(e.message)
        }
        pdfDoc.close()
        println("Файл PDF создан. Путь: ./$PDF_FILENAME")
    }

    companion object {
        const val A3_ALBUM = "1191 842"

        const val PDF_FILENAME = "output/persons.pdf"

        val fontHeader: Font = Liberation.SERIF_BOLD.create(8)
        val fontCell: Font = Liberation.SERIF.create(8)

        val columns = mapOf(
            "Имя" to 1.0F,
            "Фамилия" to 1.3F,
            "Отчество" to 1.3F,
            "Возраст" to 0.8F,
            "Пол" to 0.8F,
            "Дата рождения" to 1.5F,
            "Место рождения" to 1.7F,
            "Инн" to 1.2F,
            "Индекс" to 1.0F,
            "Страна" to 0.8F,
            "Область" to 1.5F,
            "Город" to 1.5F,
            "Улица" to 1.5F,
            "Дом" to 0.8F,
            "Квартира" to 1.0F,
        )
    }
}
