package homework4

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

class JsonFileGenerator(private val persons: MutableList<Person>) : FileGenerator {
    override fun generate() {
        createOutputDir()
        val jsonFile = File(JSON_FILENAME)
        val json = Json.encodeToString(persons)
        jsonFile.writeText(json)
        println("Файл JSON создан. Путь: ./$JSON_FILENAME")
    }

    companion object {
        const val JSON_FILENAME = "output/persons.json"
    }
}

interface FileGenerator {
    val outputDirName: String
        get() = "output"

    fun createOutputDir() {
        val outputDir = File(outputDirName)
        if (!outputDir.exists()) {
            outputDir.mkdir()
        }
    }

    fun generate()
}