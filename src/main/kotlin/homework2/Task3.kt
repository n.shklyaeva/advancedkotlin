package homework2

val sum = { a: Double, b: Double -> a + b }
val subtraction = { a: Double, b: Double -> a - b }
val multiply = { a: Double, b: Double -> a * b }
val divide = { a: Double, b: Double ->
    if (b != 0.0) {
        a / b
    } else throw NullPointerException()
}

fun main() {
    println(calculator(getCalculationMethod("+"), 2.0, 3.0))
    calculator(getCalculationMethod("-"), 2.0, 13.0)
    calculator(getCalculationMethod("*"), 2.0, 13.0)
    calculator(getCalculationMethod("/"), 2.0, 13.0)
}

fun calculator(lambda: ((Double, Double) -> Double), a: Double, b: Double) {
    println(lambda(a, b))
}

fun getCalculationMethod(name: String): (Double, Double) -> Double {
    return when (name) {
        "+" -> sum
        "-" -> subtraction
        "*" -> multiply
        "/" -> divide
        else -> throw UnsupportedOperationException()
    }
}