package homework2

fun main() {
    val myTiger = Tiger("Sherhan")
    val myLion = Lion("Mufasa")
    val mySalmon = Salmon ("Salmon")
    val myTuna = Tuna("Tuna")

    useRunSkill(myTiger)
    useRunSkill(myLion)

    useSwimSkill(myTiger)
    useSwimSkill(myLion)
    useSwimSkill(myTuna)
    useSwimSkill(mySalmon)

    useSwimAndRunSkill(myTiger)
    useSwimAndRunSkill(myLion)
}

abstract class Pet(var name: String)

interface Runnable {
    fun run()
}

interface Swimmable {
    fun swim()
}

abstract class Cat(name: String) : Pet(name), Runnable, Swimmable {
    override fun run() {
        println("I am a cat, and I'm running")
    }

    override fun swim() {
        println("I am a cat, and I'm swimming")
    }

}

abstract class Fish(name: String) : Pet(name), Swimmable {
    override fun swim() {
        println("I am a fish, and I'm swimming")
    }

}

class Tiger(name: String) : Cat(name)
class Lion(name: String) : Cat(name)
class Salmon(name: String) : Fish(name)
class Tuna(name: String) : Fish(name)

fun <T> useRunSkill(t: T) where T : Runnable {
    println("Run!")
    t.run()
}

fun <T> useSwimSkill(t: T) where T : Swimmable {
    println("Swim!")
    t.swim()
}

fun <T> useSwimAndRunSkill(t: T) where T : Runnable, T : Swimmable {
    println("Swim and run!")
    t.run()
    t.swim()
}