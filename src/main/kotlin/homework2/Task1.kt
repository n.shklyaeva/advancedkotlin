package homework2

import java.math.RoundingMode

fun main() {
    val listOfDouble1: List<Double?> = mutableListOf(13.31, 3.98, 12.0, 2.99, 9.0)
    val listOfDouble2: List<Double?> = mutableListOf(133.21, null, 233.98, null, 26.99, 5.0, 7.0, 9.0)
    println(roundNumberToTwoDecimal(sumOfCollectionElements(listOfDouble1)))
    println(roundNumberToTwoDecimal(sumOfCollectionElements(listOfDouble2)))
}

fun sumOfCollectionElements(list: List<Double?>): Double {
    return list.filterNotNull().map { if (it.toInt() % 2 == 0) it * it else it / 2 }.filter { it <= 25 }
        .sortedDescending().take(10).sum()
}

fun roundNumberToTwoDecimal(number: Double): String {
    return number.toBigDecimal().setScale(2, RoundingMode.HALF_EVEN).toString().replace(".", ",")
}

