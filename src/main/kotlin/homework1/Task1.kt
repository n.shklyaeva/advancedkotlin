package homework1

fun main() {
    val list = mutableListOf(1, 4, 9, 16, 25)
    println(list.square())
}

fun MutableList<Int>.square(): MutableList<Int> {
    val squareList = mutableListOf<Int>()
    for (item in this) {
        squareList.add(item * item)
    }
    return squareList
}
