package homework1

fun task3(vararg params: String) {
    println("Передано ${params.count()} элемента:")
    params.forEach { print("$it;") }
}

fun main() {
    task3("12", "122", "1234", "fpo")
}
