package homework1

class task5 {
    companion object {
        var counter = 0
        fun counter() {
            counter++
            println("Вызван counter. Количество вызовов = $counter")
        }
    }
}

fun main() {
    task5.counter()
    task5.counter()
    task5.counter()
    task5.counter()
    task5.counter()
}
