package homework1

fun main() {
    println(
        "Со всеми параметрами " + person(
            "Nikita",
            "Tarasov",
            "Konstantinovich",
            "Man",
            83,
            "1941 - 01 - 20",
            "1234567890",
            "345 - 111 - 255"
        )
    )
    println(
        "Только с обязательными параметрами " + person(
            "Nikita",
            "Tarasov",
            gender = "Konstantinovich",
            age = 83,
            dateOfBirth = "1941 - 01 - 20"
        )
    )
    println(
        "Произвольный порядок " + person(
            "Nikita",
            dateOfBirth = "1941 - 01 - 20",
            gender = "Man",
            age = 83,
            inn = "1234567890",
            surname = "Tarasov",
            snils = "345 - 111 - 255",
            patronymic = "Konstantinovich"
        )
    )
}
fun person(
    name: String,
    surname: String,
    patronymic: String = "",
    gender: String,
    age: Int,
    dateOfBirth: String,
    inn: String = "",
    snils: String = ""
): String {
    return "$name $surname $patronymic $gender $age $dateOfBirth $inn $snils"
}
