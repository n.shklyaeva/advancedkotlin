package homework1

data class Person(
    val name: String,
    val surName: String,
    val patronymic: String? = null,
    val gender: String,
    var age: Int,
    val dateOfBirth: String,
    val inn: String? = null,
    val SNILS: String? = null
)

fun personConfig(
    name: String,
    surName: String,
    patronymic: String? = null,
    gender: String,
    age: Int,
    dateOfBirth: String,
    inn: String? = null,
    SNILS: String? = null
): Person {
    return Person(
        name,
        surName,
        patronymic,
        gender,
        age,
        dateOfBirth,
        inn,
        SNILS
    )
}

fun main() {
    val person = personConfig(
        name = "Ivan",
        surName = "Ivanov",
        patronymic = null,
        gender = "Male",
        age = 33,
        dateOfBirth = "1990-01-15",
        inn = null,
        SNILS = null
    )
    println(person.toString())
}
